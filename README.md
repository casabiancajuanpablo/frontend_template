# README #

This is a simple frontend template created by JP Casabianca

### What is this repository for? ###

* Build frontend projects

### How do I get set up? ###

* It's really simple, just install the dependencies with npm and then run gulp watchFiles to watch for changes in styles and scripts. 
* Then, run gulp to build the dist folder.
* And VOILA! 
