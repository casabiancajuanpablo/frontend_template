'use strict';

// Requirements
	var gulp = require('gulp'),
      concat = require('gulp-concat'),
      uglify = require('gulp-uglify'),
      rename = require('gulp-rename'),
	    sass = require('gulp-sass'),
	    maps = require('gulp-sourcemaps'),
    cleancss = require('gulp-clean-css'),
         del = require('del');

// Script and Style Folder Paths
var paths = {
    scripts: ['src/assets/js/**/*.js'],
    styles: 'src/assets/styles/**/*.scss'
};

// Concat JS Scripts from Assets to Js folder
gulp.task("concatScripts", function () {
	return gulp.src(paths.scripts)
	.pipe(concat("main.js"))
	.pipe(maps.init())
	.pipe(maps.write("./"))
	.pipe(gulp.dest("src/js"))
});

// Minify javascript scripts
gulp.task("minifyScripts", ["concatScripts"], function () {
	return gulp.src("src/js/main.js")
	.pipe(uglify())
	.pipe(rename('main.min.js'))
	.pipe(gulp.dest("src/js"))
});

// Compiles SASS files to unified css file 
gulp.task("compileSass", function(){
	return gulp.src(paths.styles)
	.pipe(sass())
	.pipe(maps.write('./'))
	.pipe(gulp.dest('src/css'));
});

// Minifies CSS into style.min.css
gulp.task("minifyCss", ["compileSass"], function() {
  	return gulp.src('src/css/style.css')
    .pipe(cleancss({compatibility: 'ie8'}))
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('src/css'));
});

// Watches files for changes
gulp.task("watchFiles", function() {
	gulp.watch([paths.styles, paths.scripts], ["minifyCss", "minifyScripts"])
	gulp.watch('dist/**/*.*')
});

// Cleans scripts and styles
gulp.task("clean", function() {
	del(["dist", "src/css/style*.css*", "src/js/main*.js*"]);
});

// Builds the tasks and creates the dist folder
gulp.task("build", ["minifyScripts", "minifyCss"], function() {
	return gulp.src([
		"src/css/style.min.css", 
		"src/js/main.min.js", 
		"src/*.html",
		"src/favicon.ico",
		"src/*.txt",
		"src/.htaccess",
		"src/site.webmanifest",
		"src/img/**",
		"src/fonts/**"], { base : './src/' })
		.pipe(gulp.dest('dist'));
});

// Watches for file changes
gulp.task("serve", ["watchFiles"]);

// The default gulp build
gulp.task("default", ["clean"], function() {
	gulp.start("build")
});









